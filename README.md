**Role Based Access Control App** is a maven project 

**Libraries used**

_junit_
_mockito_
_slf4j_
_jsr305_

**To Build a project**

_Run command_

`mvn clean install`

**MemoryRBACServiceTest** 

Has all the test cases related to `access api` based on _user_ and _action type_ on _resource_

**Entities**

_User_
_Role_
_Resource_
_ActionType_

**Services**

_UserService_
_RoleService_
_ResourceService_
_RBACService_

**Data kept in in-memory and class in memory package**



