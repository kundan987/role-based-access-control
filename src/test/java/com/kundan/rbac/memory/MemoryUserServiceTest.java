package com.kundan.rbac.memory;

import com.kundan.rbac.entity.User;
import com.kundan.rbac.service.UserService;
import com.kundan.rbac.core.ServiceFactory;
import com.kundan.rbac.core.Services;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class MemoryUserServiceTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private UserService userService;

    @Before
    public void setUp() {
        final Services services = ServiceFactory.createServices();
        userService = services.getUserService();
    }

    @Test
    public void testCreateUser_duplicate() {
        final User omar = new User("kundan");
        userService.create(omar);

        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("User kundan already exists");
        userService.create(omar);
    }


    @Test
    public void testCreateUser_npe() {
        thrown.expect(NullPointerException.class);
        userService.create(null);
    }

    @Test
    public void testCreateUser_ok() {
        assertNull("kundan should not exist yet", userService.findByName("kundan"));

        final User kundan = new User("kundan");
        userService.create(kundan);

        assertEquals("kundan should exist now", kundan, userService.findByName("kundan"));
    }

    @Test
    public void testDeleteUser_notExists() {
        assertNull("kundan should not exist yet", userService.findByName("kundan"));

        final User kundan = new User("kundan");
        userService.delete(kundan);

        assertNull("kundan still should not exist", userService.findByName("kundan"));
    }

    @Test
    public void testDeleteUser_npe() {
        thrown.expect(NullPointerException.class);
        userService.delete(null);
    }

    @Test
    public void testDeleteUser_ok() {
        final User kundan = new User("kundan");

        userService.create(kundan);
        assertEquals("kundan should exist", kundan, userService.findByName("kundan"));

        userService.delete(kundan);
        assertNull("kundan should be deleted", userService.findByName("kundan"));
    }
}
