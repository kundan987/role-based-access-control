package com.kundan.rbac.memory;

import com.kundan.rbac.core.ServiceFactory;
import com.kundan.rbac.core.Services;
import com.kundan.rbac.entity.ActionType;
import com.kundan.rbac.entity.Resource;
import com.kundan.rbac.entity.Role;
import com.kundan.rbac.entity.User;
import com.kundan.rbac.service.RBACService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MemoryRBACServiceTest {

    private static final User KUNDAN = new User("kundan");
    private static final User RIMMY = new User("rimmy");
    private static final User KIMMY = new User("kimmy");
    private static final User VICKY = new User("vicky");
    private static final User NOBODY = new User("nobody");

    private static final Role ADMIN = new Role("admin", new ArrayList<>(Arrays.asList(ActionType.READ, ActionType.WRITE, ActionType.DELETE)));
    private static final Role MANAGER = new Role("manager", new ArrayList<>(Arrays.asList(ActionType.READ, ActionType.WRITE)));
    private static final Role INTERN = new Role("intern", new ArrayList<>(Arrays.asList(ActionType.READ)));
    private static final Role NOROLE = new Role("norole", new ArrayList<>(Arrays.asList()));

    private static final Resource CONTRACT = new Resource("contract");
    private static final Resource JOURNAL = new Resource("journal");
    private static final Resource USERDETAIL = new Resource("userDetail");
    private static final Resource NORESOURCE = new Resource("noresource");

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private RBACService rbacService;

    final Services services = ServiceFactory.createServices();

    @Before
    public void setUp() {
        services.getUserService().create(KUNDAN);
        services.getUserService().create(RIMMY);
        services.getUserService().create(KIMMY);
        services.getUserService().create(VICKY);

        services.getRoleService().create(ADMIN);
        services.getRoleService().create(MANAGER);
        services.getRoleService().create(INTERN);

        services.getResourceService().create(CONTRACT);
        services.getResourceService().create(JOURNAL);
        services.getResourceService().create(USERDETAIL);

        rbacService = services.getMembershipService();
    }

    @Test
    public void addUserToRole_duplicate() {
        rbacService.addUserToRole(RIMMY, MANAGER);
        rbacService.addUserToRole(KIMMY, MANAGER);

        final Set<User> expected = new HashSet<>();
        expected.add(RIMMY);
        expected.add(KIMMY);
        assertEquals(sorted(expected), sorted(rbacService.getUsersInRole(MANAGER)));

        rbacService.addUserToRole(RIMMY, MANAGER);
        assertEquals(sorted(expected), sorted(rbacService.getUsersInRole(MANAGER)));
    }

    @Test
    public void addUserToRole_noSuchRole() {
        thrown.expect(IllegalArgumentException.class);
        rbacService.addUserToRole(RIMMY, NOROLE);
    }

    @Test
    public void addUserToRole_noSuchUser() {
        thrown.expect(IllegalArgumentException.class);
        rbacService.addUserToRole(NOBODY, MANAGER);
    }

    @Test
    public void addUserToRole_npeRole() {
        thrown.expect(NullPointerException.class);
        rbacService.addUserToRole(KIMMY, null);
    }

    @Test
    public void addUserToRole_npeUser() {
        thrown.expect(NullPointerException.class);
        rbacService.addUserToRole(null, MANAGER);
    }

    @Test
    public void testRemoveUserFromRole() {
        rbacService.addUserToRole(KUNDAN, ADMIN);
        rbacService.addUserToRole(RIMMY, MANAGER);
        assertTrue("kundan is an admin", rbacService.isUserInRole(KUNDAN, ADMIN));

        rbacService.removeUserFromRole(KUNDAN, ADMIN);
        assertFalse("kundan is not an admin anymore", rbacService.isUserInRole(KUNDAN, ADMIN));
    }

    @Test
    public void testIsUserInRole_no() {
        rbacService.addUserToRole(KUNDAN, ADMIN);
        rbacService.addUserToRole(RIMMY, MANAGER);

        assertFalse("kundan is not a manager", rbacService.isUserInRole(KUNDAN, MANAGER));
        assertFalse("rimmy is not an admin", rbacService.isUserInRole(RIMMY, ADMIN));
    }

    @Test
    public void testIsUserInRole_yes() {
        rbacService.addUserToRole(KUNDAN, ADMIN);
        rbacService.addUserToRole(RIMMY, MANAGER);

        assertTrue("kundan is an admin", rbacService.isUserInRole(KUNDAN, ADMIN));
        assertTrue("rimmy is a manager", rbacService.isUserInRole(RIMMY, MANAGER));
    }

    @Test
    public void testAccess_yes(){
        rbacService.addUserToRole(KUNDAN, ADMIN);
        rbacService.addUserToRole(VICKY, INTERN);
        rbacService.addResourceToRole(USERDETAIL, ADMIN);
        rbacService.addResourceToRole(JOURNAL, INTERN);
        assertTrue("kundan is an admin and has access to delete userdetail", rbacService.access(KUNDAN, ActionType.DELETE, USERDETAIL));
        assertTrue("vicky is an intern and has access to read journal", rbacService.access(VICKY, ActionType.READ, JOURNAL));

    }

    @Test
    public void testAccess_no(){
        rbacService.addUserToRole(RIMMY, MANAGER);
        rbacService.addUserToRole(VICKY, INTERN);
        rbacService.addResourceToRole(CONTRACT, MANAGER);
        rbacService.addResourceToRole(JOURNAL, INTERN);
        assertFalse("rimmy is a manager and has not access to delete contract", rbacService.access(RIMMY, ActionType.DELETE, CONTRACT));
        assertFalse("vicky is an intern and has not access to write journal", rbacService.access(VICKY, ActionType.WRITE, JOURNAL));
    }

    @Test
    public void testAccess_noSuchResource() {
        thrown.expect(IllegalArgumentException.class);
        rbacService.access(KUNDAN, ActionType.DELETE, NORESOURCE);
    }

    @Test
    public void testAccess_noSuchUser() {
        thrown.expect(IllegalArgumentException.class);
        rbacService.access(NOBODY, ActionType.DELETE, NORESOURCE);
    }

    private static <T extends Comparable<T>> List<T> sorted(Collection<T> items) {
        final List<T> list = new ArrayList<>(items);
        Collections.sort(list);
        return list;
    }


}