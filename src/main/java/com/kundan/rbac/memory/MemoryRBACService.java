package com.kundan.rbac.memory;

import com.kundan.rbac.entity.ActionType;
import com.kundan.rbac.entity.Resource;
import com.kundan.rbac.entity.Role;
import com.kundan.rbac.service.RBACService;
import com.kundan.rbac.entity.User;
import com.kundan.rbac.core.AbstractService;
import com.kundan.rbac.core.Services;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.*;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of the RBAC service that stores user, role and resource relationships in memory.
 */
@ParametersAreNonnullByDefault
public class MemoryRBACService extends AbstractService implements RBACService {
    private static final Logger LOG = LoggerFactory.getLogger(MemoryRBACService.class);

    private final Map<Role, Set<User>> usersByRole = new HashMap<>();
    private final Map<Role, Set<Resource>> resourcesByRole = new HashMap<>();

    public MemoryRBACService(Services services) {
        super(services);
    }

    public void addUserToRole(User user, Role role) {
        requireExists(user);
        requireExists(role);

        Set<User> users = usersByRole.get(role);
        if (users == null) {
            users = new HashSet<>();
            usersByRole.put(role, users);
        }
        users.add(user);

        LOG.debug("Added user " + user + " to role " + role);
    }

    public boolean isUserInRole(User user, Role role) {
        requireNonNull(user, "user");
        requireNonNull(role, "role");

        return getUsersInRole(role).contains(user);
    }

    public Collection<User> getUsersInRole(Role role) {
        requireNonNull(role, "role");

        final Collection<User> users = usersByRole.getOrDefault(role, new HashSet<>());

        LOG.debug("Current users in role {}: {}", role.toString(), users.toString());

        return users;
    }

    public void removeUserFromRole(User user, Role role) {
        requireNonNull(user, "user");
        requireNonNull(role, "role");

        getUsersInRole(role).remove(user);
        LOG.debug(String.format("Removed user %s from role %s", user, role));
    }

    public void removeRole(Role role){
        requireNonNull(role, "role");
        usersByRole.remove(role);
    }

    public void addResourceToRole(Resource resource, Role role) {
        requireExists(resource);
        requireExists(role);

        Set<Resource> resources = resourcesByRole.get(role);
        if (resources == null) {
            resources = new HashSet<>();
            resourcesByRole.put(role, resources);
        }
        resources.add(resource);

        LOG.debug("Added resource " + resource + " to role " + role);
    }

    public boolean isResourceInRole(Resource resource, Role role) {
        requireNonNull(resource, "resource");
        requireNonNull(role, "role");

        return getResourcesInRole(role).contains(resource);
    }

    public Collection<Resource> getResourcesInRole(Role role) {
        requireNonNull(role, "role");

        final Collection<Resource> users = resourcesByRole.getOrDefault(role, new HashSet<>());

        LOG.debug("Current users in role {}: {}", role.toString(), users.toString());

        return users;
    }

    public void removeResourceFromRole(Resource resource, Role role) {
        requireNonNull(resource, "resource");
        requireNonNull(role, "role");

        getResourcesInRole(role).remove(resource);
        LOG.debug(String.format("Removed resource %s from role %s", resource, role));
    }

    public boolean access(User user, ActionType actionType, Resource resource) {
        requireExists(user);
        requireExists(resource);
        for (Map.Entry<Role, Set<User>> map : usersByRole.entrySet()){
            if (map.getKey().getActionTypes().contains(actionType) && map.getValue().contains(user)){
                if (resourcesByRole.get(map.getKey()).contains(resource)){
                    return true;
                }
            }
        }

        return false;
    }

    private void requireExists(User user) {
        requireNonNull(user, "user");
        if (services.getUserService().findByName(user.getName()) == null) {
            throw new IllegalArgumentException("User '" + user + "' does not exist!");
        }
    }

    private void requireExists(Role role) {
        requireNonNull(role, "role");
        if (services.getRoleService().findByName(role.getName()) == null) {
            throw new IllegalArgumentException("Role '" + role + "' does not exist!");
        }
    }

    private void requireExists(Resource resource) {
        requireNonNull(resource, "resource");
        if (services.getResourceService().findByName(resource.getName()) == null) {
            throw new IllegalArgumentException("Resource '" + resource + "' does not exist!");
        }
    }
}
