package com.kundan.rbac.memory;

import com.kundan.rbac.entity.Resource;
import com.kundan.rbac.service.ResourceService;
import com.kundan.rbac.core.AbstractService;
import com.kundan.rbac.core.Services;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of the resource service that stores all resources in memory.
 */
@ParametersAreNonnullByDefault
public class MemoryResourceService extends AbstractService implements ResourceService {
    private static final Logger LOG = LoggerFactory.getLogger(MemoryResourceService.class);

    private final Map<String, Resource> resources = new HashMap<>();

    public MemoryResourceService(Services services) {
        super(services);
    }

    public Resource findByName(String name) {
        requireNonNull(name, "name");
        return resources.get(name);
    }

    public void create(Resource resource) {
        requireNonNull(resource, "resource");
        if (resources.containsKey(resource.getName())) {
            throw new IllegalArgumentException("Resource " + resource.getName() + " already exists");
        }
        resources.put(resource.getName(), resource);
        LOG.debug("Created resource: {}", resource.getName());
    }

    public void delete(Resource resource) {
        requireNonNull(resource, "resource");
        resources.remove(resource.getName());
        LOG.debug("Deleted resource: {}", resource.getName());
    }
}
