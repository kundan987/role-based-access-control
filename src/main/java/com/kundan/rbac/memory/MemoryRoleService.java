package com.kundan.rbac.memory;

import com.kundan.rbac.entity.Role;
import com.kundan.rbac.service.RoleService;
import com.kundan.rbac.core.AbstractService;
import com.kundan.rbac.core.Services;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * An implementation of the role service that stores all roles in memory.
 */
@ParametersAreNonnullByDefault
public class MemoryRoleService extends AbstractService implements RoleService {
    private static final Logger LOG = LoggerFactory.getLogger(MemoryRoleService.class);

    private final Map<String, Role> roles = new HashMap<>();

    public MemoryRoleService(Services services) {
        super(services);
    }

    public Role findByName(String name) {
        requireNonNull(name, "name");
        return roles.get(name);
    }

    public void create(Role role) {
        requireNonNull(role, "role");
        if (roles.containsKey(role.getName())) {
            throw new IllegalArgumentException("Role " + role.getName() + " already exists");
        }
        roles.put(role.getName(), role);
        LOG.debug("Created role: {}", role.getName());
    }

    public void delete(Role role) {
        requireNonNull(role, "role");
        roles.remove(role.getName());
        LOG.debug("Deleted role: {}", role.getName());
    }
}
