package com.kundan.rbac.entity;

public enum ActionType {

    READ, WRITE, DELETE
}
