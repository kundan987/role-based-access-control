package com.kundan.rbac.entity;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static java.util.Objects.requireNonNull;

/**
 * A resource that may or may not belong to any roles.
 */
@ParametersAreNonnullByDefault
public class Resource implements Comparable<Resource> {
    private final String name;

    public Resource(String name) {
        this.name = requireNonNull(name, "name");
    }

    public String getName() {
        return name;
    }

    public int compareTo(@Nonnull Resource other) {
        return name.compareTo(other.name);
    }

    @Override
    public String toString() {
        return name;
    }
}
