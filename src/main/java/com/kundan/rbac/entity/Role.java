package com.kundan.rbac.entity;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.requireNonNull;

/**
 * A role that may or may not have any users in it.
 */
@ParametersAreNonnullByDefault
public class Role implements Comparable<Role> {

    private final String name;

    private List<ActionType> actionTypes;

    public Role(String name, List<ActionType> actionTypes) {
        this.name = requireNonNull(name, "name");
        this.actionTypes = actionTypes;
    }

    public String getName() {
        return name;
    }

    public List<ActionType> getActionTypes() {
        return actionTypes;
    }

    public void setActionTypes(List<ActionType> actionTypes) {
        this.actionTypes = actionTypes;
    }

    public int compareTo(@Nonnull Role other) {
        return name.compareTo(other.name);
    }

    @Override
    public String toString() {
        return name;
    }
}
