package com.kundan.rbac.core;


import com.kundan.rbac.service.ResourceService;
import com.kundan.rbac.service.RoleService;
import com.kundan.rbac.service.RBACService;
import com.kundan.rbac.service.UserService;

/**
 * Provides access to all of the services so that circular dependencies between them can be resolved.
 */
public interface Services {
    RoleService getRoleService();

    UserService getUserService();

    ResourceService getResourceService();

    RBACService getMembershipService();
}
