package com.kundan.rbac.core;

import com.kundan.rbac.service.ResourceService;
import com.kundan.rbac.service.RoleService;
import com.kundan.rbac.service.RBACService;
import com.kundan.rbac.service.UserService;
import com.kundan.rbac.memory.MemoryResourceService;
import com.kundan.rbac.memory.MemoryRoleService;
import com.kundan.rbac.memory.MemoryRBACService;
import com.kundan.rbac.memory.MemoryUserService;

/**
 * Factory for building a new memory-based users, roles, resources service.
 */
public class ServiceFactory implements Services {
    private final RoleService roleService;
    private final UserService userService;
    private final ResourceService resourceService;
    private final RBACService membershipService;

    public static Services createServices() {
        return new ServiceFactory();
    }

    private ServiceFactory() {
        roleService = new MemoryRoleService(this);
        userService = new MemoryUserService(this);
        resourceService = new MemoryResourceService(this);
        membershipService = new MemoryRBACService(this);
    }

    public RoleService getRoleService() {
        return roleService;
    }

    public UserService getUserService() {
        return userService;
    }

    public RBACService getMembershipService() {
        return membershipService;
    }

    public ResourceService getResourceService() {
        return resourceService;
    }
}
