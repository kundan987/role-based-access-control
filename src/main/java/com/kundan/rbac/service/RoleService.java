package com.kundan.rbac.service;

import com.kundan.rbac.entity.Role;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * A service that keeps track of which roles exist.
 */
@ParametersAreNonnullByDefault
public interface RoleService {

    @Nullable
    Role findByName(String name);

    void create(Role role);

    void delete(Role role);
}
