package com.kundan.rbac.service;

import com.kundan.rbac.entity.User;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * A service that keeps track of which users exist.
 */
@ParametersAreNonnullByDefault
public interface UserService {

    @Nullable
    User findByName(String name);

    void create(User user);

    void delete(User user);
}
