package com.kundan.rbac.service;

import com.kundan.rbac.entity.ActionType;
import com.kundan.rbac.entity.Resource;
import com.kundan.rbac.entity.Role;
import com.kundan.rbac.entity.User;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Collection;

/**
 * A service that associates users with the roles and the resources that they belong to.
 */
@ParametersAreNonnullByDefault
public interface RBACService {

    void addUserToRole(User user, Role role);

    boolean isUserInRole(User user, Role role);

    Collection<User> getUsersInRole(Role role);

    void removeUserFromRole(User user, Role role);

    void removeRole(Role role);

    void addResourceToRole(Resource resource, Role role);

    boolean isResourceInRole(Resource resource, Role role);

    Collection<Resource> getResourcesInRole(Role role);

    void removeResourceFromRole(Resource resource, Role role);

    boolean access(User user, ActionType actionType, Resource resource);

}
