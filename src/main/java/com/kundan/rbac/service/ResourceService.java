package com.kundan.rbac.service;

import com.kundan.rbac.entity.Resource;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;

/**
 * A service that keeps track of which resource exist.
 */
@ParametersAreNonnullByDefault
public interface ResourceService {

    @Nullable
    Resource findByName(String name);

    void create(Resource resource);

    void delete(Resource resource);
}
